﻿using reactql.Application.Common.Interfaces;
using System;

namespace reactql.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
