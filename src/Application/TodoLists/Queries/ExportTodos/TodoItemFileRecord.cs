﻿using reactql.Application.Common.Mappings;
using reactql.Domain.Entities;

namespace reactql.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
