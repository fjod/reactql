﻿using reactql.Domain.Common;
using System.Threading.Tasks;

namespace reactql.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
