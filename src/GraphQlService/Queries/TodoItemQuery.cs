﻿using System.Linq;
using GraphQL;
using GraphQL.Types;
using GraphQlService.Types;
using reactql.Application.Common.Interfaces;
using reactql.Domain.Entities;

namespace GraphQlService.Queries
{
    public class TodoItemQuery : ObjectGraphType<TodoItem>
    {
        public TodoItemQuery(IApplicationDbContext context)
        {
            Field<ListGraphType<TodoItemType>>("items", "", null,
                c => 
                    context.TodoItems.ToList());
            
            Field<TodoItemType>("item","", new QueryArguments(
                new QueryArgument<IntGraphType>{Name = "id"}), c =>
            {
                var id = c.GetArgument<int>("id");
                return context.TodoItems.FirstOrDefault(item => item.Id == id);
            });
        }
    }
}