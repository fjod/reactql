﻿using System;
using GraphQL.Types;
using GraphQlService.Types;
using reactql.Domain.Entities;
using reactql.Domain.ValueObjects;

namespace GraphQlService.Queries
{
    public class GraphQLDemoSchema:Schema
    {
        public GraphQLDemoSchema(IServiceProvider  resolver):base(resolver)
        {
            Query = resolver.GetService(typeof(TodoItemQuery)) as TodoItemQuery;
            RegisterTypeMapping(typeof(TodoList),typeof(TodoListType));
            RegisterTypeMapping(typeof(Colour),typeof(ColourType));
        }
    }
}
//https://www.red-gate.com/simple-talk/development/dotnet-development/building-and-consuming-graphql-api-in-asp-net-core-3-1/