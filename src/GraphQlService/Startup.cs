using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using GraphiQl;
using GraphQL;
using GraphQL.Server;
using GraphQL.SystemTextJson;
using GraphQL.Types;
using GraphQlService.Queries;
using GraphQlService.Types;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using reactql.Application.Common.Interfaces;
using reactql.Domain.Enums;
using reactql.Infrastructure.Identity;
using reactql.Infrastructure.Persistence;

namespace GraphQlService
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            services.AddCors();
            services.AddTransient<IDateTime, DateTimeService>();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddSingleton<ICurrentUserService, CurrentUserService>();
            services.AddScoped<IDocumentExecuter, DocumentExecuter>();
            services.AddScoped<IDocumentWriter, DocumentWriter>();
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseInMemoryDatabase("reactqlDb"));
            services.AddScoped<IDomainEventService, DomainEventService>();
            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());
            services
                .AddDefaultIdentity<ApplicationUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddScoped<TodoItemQuery>();
            services.AddScoped<TodoItemType>();
            services.AddScoped<ColourType>();
            services.AddScoped<EnumerationGraphType<PriorityLevel>>();
            services.AddScoped<TodoListType>();
            services.AddScoped<ISchema, GraphQLDemoSchema>();
            services.AddScoped<GraphQLDemoSchema>();
            
            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.WriteIndented = true;
                    options.JsonSerializerOptions.Converters.Add(new CustomJsonConverterForType());
                });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "GraphQlService", Version = "v1"});
            });

            services
                .AddGraphQL()
                // Adds all graph types in the current assembly with a singleton lifetime.
                .AddGraphTypes()
                // Add GraphQL data loader to reduce the number of calls to our repository. https://graphql-dotnet.github.io/docs/guides/dataloader/
                .AddDataLoader().AddSystemTextJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseGraphiQl("/graphql");
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true) // allow any origin
                .AllowCredentials()); // allow credentials
            
            app.UseAuthorization();
            app.UseGraphQL<GraphQLDemoSchema>();
          
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}