﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using reactql.Application.Common.Interfaces;
using reactql.Application.Common.Models;
using reactql.Domain.Common;

namespace GraphQlService
{
    public class DomainEventService : IDomainEventService
    {
        private readonly ILogger<reactql.Infrastructure.Services.DomainEventService> _logger;
        private readonly IPublisher _mediator;

        public DomainEventService(ILogger<reactql.Infrastructure.Services.DomainEventService> logger, IPublisher mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public async Task Publish(DomainEvent domainEvent)
        {
            _logger.LogInformation("Publishing domain event. Event - {event}", domainEvent.GetType().Name);
            await _mediator.Publish(GetNotificationCorrespondingToDomainEvent(domainEvent));
        }

        private INotification GetNotificationCorrespondingToDomainEvent(DomainEvent domainEvent)
        {
            return (INotification)Activator.CreateInstance(
                typeof(DomainEventNotification<>).MakeGenericType(domainEvent.GetType()), domainEvent);
        }
    }
}