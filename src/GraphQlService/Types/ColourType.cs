﻿using GraphQL.Types;
using reactql.Domain.ValueObjects;

namespace GraphQlService.Types
{
    public class ColourType  : ObjectGraphType<Colour>
    {
        public ColourType()
        {
            Name = nameof(Colour);
            Field(_ => _.Code).Description("code");
        }
    }
}