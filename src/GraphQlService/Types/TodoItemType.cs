﻿using System.Collections.Generic;
using GraphQL.Types;
using reactql.Domain.Entities;
using reactql.Domain.Enums;

namespace GraphQlService.Types
{
    public class TodoItemType : ObjectGraphType<TodoItem>
    {
        public TodoItemType()
        {
            Name = nameof(TodoItem);
            Field(_ => _.Id).Description("Item's Id");
            Field(_ => _.Done).Description("If this item is done");
            Field(_ => _.Note).Description("Description");
            Field(_ => _.Priority).Description("How important it is");
            Field(_ => _.Title).Description("Title");
            Field(_ => _.ListId).Description("List id");
            Field(_ => _.Created).Description("Created datetime");
            Field(_ => _.CreatedBy).Description("Author");
            Field(_ => _.LastModifiedBy).Description("Modifier");
        }
    }
}