﻿using GraphQL.Types;
using reactql.Domain.Entities;

namespace GraphQlService.Types
{
    public class TodoListType : ObjectGraphType<TodoList>
    {
        public TodoListType()
        {
            Name = nameof(TodoList);
            Field(_ => _.Id).Description("Item's Id");
            Field(_ => _.Colour).Description("Colour");
            Field(_ => _.Title).Description("Title");
            Field(_ => _.Created).Description("Created datetime");
            Field(_ => _.CreatedBy).Description("Author");
            Field(_ => _.LastModifiedBy).Description("Modifier");
        }
    }
}