﻿using GraphQL.Types;
using reactql.Domain.Enums;

namespace GraphQlService.Types
{
    public class PriorityLevelType : EnumerationGraphType<PriorityLevel>
    {
        public PriorityLevelType()
        {
            Name = nameof(PriorityLevel);
        }
    }
}